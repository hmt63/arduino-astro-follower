Astro follower

Coposants

 * Arduino UNO
 * dfROBOT LCD shield [lien](https://wiki.dfrobot.com/LCD_KeyPad_Shield_For_Arduino_SKU__DFR0009#target_5)
 * stepper module : ULN2003 [](https://forum.fritzing.org/t/28byj-48-stepper-motor-and-uln2003-driver-module/12883)
 * 5V Stepper motor : 28BYJ-48 Stepper Motor 

Première version avec un axe droit
![](images/photo-astro-28BYJ-48-driver_and_motor-2_bb.png "Schéma")
![alt text](images/IMG_1572.jpeg "Title")
![alt text](images/IMG_1573.jpeg "Title")
![alt text](images/IMG_1574.jpeg "Title")

Seconde version avec un axe courbé. Ne fonctionne pas, car la courbure n'est pas régulière.

![alt text](images/IMG_1605.jpeg "Title")
![alt text](images/IMG_1606.jpeg "Title")

