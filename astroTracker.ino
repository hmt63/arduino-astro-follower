// Arduino stepper motor control code

#include <Stepper.h>
// https://wiki.dfrobot.com/LCD_KeyPad_Shield_For_Arduino_SKU__DFR0009#target_5
#include <LiquidCrystal.h>

#define DEBUG

#ifdef DEBUG
#define LOG(A) Serial.println(A)
#else
#define LOG(A)
#endif

// stepper 28BYJ-48
// max speed 10 rpm
// use 5 rpm

const int stepsPerRevolution =  64 * 32 ;
//Stepper stepper(stepsPerRevolution,4,6,5,7); // permute 6 and 5 for stepper 28BYJ-48

// use with LCD shield DFR0009
// cf // permute 6 and 5 for stepper 28BYJ-48
// permute 12 and 11 for stepper 28BYJ-48
Stepper stepper(stepsPerRevolution, 3, 12, 11, 13);

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);           // select the pins used on the LCD panel

// ============== divers
const int direction = -1 ; // direction de rotation 1 si vis mobile, -1 si ecrou mobile

// define some values used by the panel and buttons
int lcd_key     = 0;
int adc_key_in  = 0;

const int btnRIGHT =  0;
const int btnUP     = 1;
const int btnDOWN   = 2;
const int btnLEFT   = 3;
const int btnSELECT = 4;
const int btnNONE   = 5;

const int rewindSpeed = 12 ;
const int normalSpeed = 5 ;

// -=============gestion de l'automate

int etat = 0 ;

const int etatInerte = 0 ;
const int etatAvance = 3 ;

const int transitionNone = 0 ;
const int transitionStart = 1 ;
const int transitionStop = 2 ;
const int transitionRewind = 3 ;
const int transitionPetitPas = 4 ;
const int transitionGrandPas = 5 ;

int trans = transitionNone ;

// ======== GEOMETRIE DU SYSTEME
float distanceToCenter = 0.175; // distance entre le centre et l'axe de rotation en metres
float firstGear = 14 ; // nombre de detns engrenage 1 (cote moteur)
float secondGear = 45 ; // nombre de detns engrenage 2 (cote entrainement)
int tempsGrandPas = 60 ; // time in seconds for a full cycle
int tempsPetitPas = 5 ; // time  bettween to action on motor
float pasFiletage = 1.0 / 1000.0 ; // pas du filetage exprim� en metre

// M5 pas = 0.8 mm
// M6 pas = 1 mm
// M8 pas = 1,25 mm

// === donn�es calcul�e
float toursParMinute = 0.0 ; // calcul�
int rotationsParPetitPas = 0  ;
int rotationsComplementairesGrandPas = 0;
unsigned long  millisPetitPas = tempsPetitPas * 1000 ;
unsigned long  millisGrandPas = tempsGrandPas * 1000 ;
unsigned long  millisPourPetitPasSuivant = millisPetitPas  ;
unsigned long  millisPourGrandPas = millisGrandPas  ;
long nombrePetitPas = tempsGrandPas / tempsPetitPas ;
long compteurPetitPas = 0 ;
bool petitPas = false ;
bool grandPas  = false ;

void setup() {
  // set the speed :
  stepper.setSpeed(normalSpeed);
  // initialize the serial port:
  Serial.begin(9600);

  // lcd start
  lcd.begin(16, 2);               // start the library
  lcd.setCursor(0, 0);            // set the LCD cursor   position
  lcd.print("Suivi astro pret");  // print a simple message on the LCD

  etat  = etatInerte ;
  lcd.setCursor(0, 1);
  lcd_key = read_LCD_buttons();
}

void loop() {

  //Serial.print (millis()) ;Serial.println (" loop") ;
  lcd.setCursor(0, 1);
  lcd_key = read_LCD_buttons();
  showStatus(lcd_key) ;
  lcd.setCursor(0, 1);
  trans = transitionCheck () ;
  stateTransition (trans) ;

}

int transitionCheck () {
  lcd_key = read_LCD_buttons();
  if (lcd_key == btnLEFT)
    return (transitionRewind);
  if (lcd_key == btnRIGHT)
    return (transitionStart);
  if (lcd_key == btnSELECT)
    return (transitionStop);
  if (grandPas == true )
    return (transitionGrandPas) ;
  if (petitPas == true )
    return (transitionPetitPas) ;
}

void stateTransition (int transition) {
  //Serial.print ("etat = ") ; Serial.println (etat) ;
  if (etat == etatInerte ) {
    if (transition == transitionRewind) {
      actionRewind();
    } else if (transition == transitionStart) {
      etat = etatAvance ;
      actionStart();
    }
    return ;
  }
  if (etat == etatAvance )  {
    if (transition ==  transitionStop ) {
      etat = etatInerte ;
      actionStop();
    } else if (transition ==  transitionRewind ) {
      etat = etatInerte ;
      actionRewind();
    } else if (transition ==  transitionPetitPas ) {
      actionPerformNormalFraction() ;
    } else if (transition ==  transitionGrandPas ) {
      actionPerformFinalFraction() ;
    }
    return ;
  } // end etatAvance
  return ;
}

void actionRewind() {
  Serial.print (millis()) ; Serial.println(" rewind   ");
  stepper.setSpeed(rewindSpeed);
  stepper.step (-1 * direction * 64);
}

void actionStart() {
  Serial.print (millis()) ; Serial.println (" start") ;
  // init values
  millisPetitPas = tempsPetitPas * 1000 ;
  millisGrandPas = tempsGrandPas * 1000 ;
  Serial.print ("duree pas =") ;  Serial.println( tempsGrandPas / tempsPetitPas ) ;
  Serial.print ("duree pas millis =") ;  Serial.println( millisPetitPas ) ;
  millisPourPetitPasSuivant = millisPetitPas  ;
  millisPourGrandPas = millisGrandPas  ;
  nombrePetitPas = tempsGrandPas / tempsPetitPas ;
  compteurPetitPas = 0 ;

  float minutesDansUneJournee = 24 * 60 ;
  float reduction = ( (1.0 * firstGear) / (1.0 * secondGear) ) ;

  toursParMinute = ((2 * PI * distanceToCenter  ) / (minutesDansUneJournee)) * ( 1.0 / pasFiletage ) / (firstGear / secondGear) ;
  Serial.print ("tours par minute =") ;  Serial.println( toursParMinute ) ;
  // division euclidienne
  int stepsParMinutes  = toursParMinute * stepsPerRevolution ;
  rotationsParPetitPas = stepsParMinutes / nombrePetitPas ;
  rotationsComplementairesGrandPas = stepsParMinutes - (rotationsParPetitPas * nombrePetitPas) ;
  stepper.setSpeed(normalSpeed);
  petitPas = true ;
  grandPas  = false ;
}

void actionStop() {
  Serial.print (millis()) ; Serial.println ("stop") ;
  petitPas = false ;
  grandPas  = false ;
}

void actionPerformNormalFraction() {
  unsigned long  startTime = millis() ;

  Serial.print (millis()) ; Serial.print(" fraction "  );Serial.println(compteurPetitPas );
  //Serial.print ("start time ") ; Serial.println (startTime);
  Serial.print ("rotation "); Serial.println (rotationsParPetitPas );
  
  stepper.step (direction * rotationsParPetitPas ) ;
  unsigned long  rotationDuration = millis() - startTime ;
  Serial.print ("delay "); Serial.println (millisPetitPas - rotationDuration);
  delay (millisPetitPas - rotationDuration ) ;

  compteurPetitPas += 1 ;
  if (nombrePetitPas == compteurPetitPas ) {
    Serial.println ("bascule fraction") ;
    petitPas = false ;
    grandPas = true ;
  } else {
    petitPas = true ;
    grandPas = false ;
  }
  Serial.println ("fin fraction" ) ;
}

void actionPerformFinalFraction() {
  Serial.print (millis()) ;  Serial.println (" final fraction" );
  unsigned long  startTime = millis() ;
  Serial.print ("rotation "); Serial.println (rotationsParPetitPas + rotationsComplementairesGrandPas);
  stepper.step (direction * ( rotationsParPetitPas + rotationsComplementairesGrandPas) ) ;
  int rotationDuration = millis() - startTime ;
  delay (millisPetitPas - rotationDuration ) ;
  compteurPetitPas = 0 ;
  petitPas = true ;
  grandPas = false ;
  Serial.println ("fin final fraction" ) ;
}


void showStatus(int keyRead) {
  if (keyRead == btnNONE)
    return ;
  switch (keyRead) {              // depending on which button was pushed, we perform an action

    case btnRIGHT: {            //  push button "RIGHT" and show the word on the screen
        lcd.print("SUIVI >    ");
        break;
      }
    case btnLEFT: {
        lcd.print("RETOUR <<  "); //  push button "LEFT" and show the word on the screen
        break;
      }
    case btnUP: {
        lcd.print("UP          ");  //  push button "UP" and show the word on the screen
        break;
      }
    case btnDOWN: {
        lcd.print("DOWN        ");  //  push button "DOWN" and show the word on the screen
        break;
      }
    case btnSELECT: {
        lcd.print("STOP        ");  //  push button "SELECT" and show the word on the screen
        break;
      }
    case btnNONE: {
        lcd.print("NONE       ");  //  No action  will show "None" on the screen
        break;
      }
    default: {
        break ;
      }
  } // end switch
  lcd.setCursor(0, 0);
}

int read_LCD_buttons() {              // read the buttons
  adc_key_in = analogRead(0);       // read the value from the sensor
  // my buttons when read are centered at these valies: 0, 144, 329, 504, 741
  // we add approx 50 to those values and check to see if we are close
  // We make this the 1st option for speed reasons since it will be the most likely result
  if (adc_key_in > 1000) return btnNONE;
  // For V1.1 us this threshold
  /*
    if (adc_key_in < 50)   return btnRIGHT;
    if (adc_key_in < 250)  return btnUP;
    if (adc_key_in < 450)  return btnDOWN;
    if (adc_key_in < 650)  return btnLEFT;
    if (adc_key_in < 850)  return btnSELECT;
  */
  // For V1.0 comment the other threshold and use the one below:

  if (adc_key_in < 50)   return btnRIGHT;
  if (adc_key_in < 195)  return btnUP;
  if (adc_key_in < 380)  return btnDOWN;
  if (adc_key_in < 555)  return btnLEFT;
  if (adc_key_in < 790)  return btnSELECT;


  return btnNONE;                // when all others fail, return this.
}

